import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter } from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore} from  'redux';
import rootReducer  from './Reducers';
import 'semantic-ui-css/semantic.min.css';

const store = createStore(rootReducer);
console.log("the state from store")
console.log(store.getState());

ReactDOM.render((
  <BrowserRouter><Provider store={store}><App /></Provider></BrowserRouter>
), document.getElementById('root'));
registerServiceWorker();
