"use strict";
import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel, Form ,Table} from 'react-bootstrap';
import IconButton from 'material-ui/IconButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Ionicon from 'react-ionicons';
import './divstyles.css';
import { Link } from 'react-router-dom';
import {SideMenu, Item} from 'react-sidemenu';
import  { Redirect } from 'react-router-dom';
import "./main.css";
 
class Adminoptions extends Component{
    constructor(props){
		super(props);
        this.handleChange=this.handleChange.bind(this);
		this.handleSubmit=this.handleSubmit.bind(this);
        this.links=this.links.bind(this);
        this.state = {
        
			};
    }
    
    
    componentDidMount(){ 
}
    
    
    handleSubmit(e){
		console.log('submited');
		e.preventDefault();   
	}
    
	handleChange(e){
        console.log('data changed');
        this.setState({
            [e.target.name]: e.target.value
            });
    }

 
 links(val){
  
     console.log("inside links")
     console.log(val)
     //add switch to route the pages
     if(val==="Table_Types")
         {
             console.log("link to Table_Types")
         //  this.props.history.push('/Table_Types')
          
         }
 }
 

    
    render () {
        return(
            <div class="padding">
	      <SideMenu onMenuItemClick={(value) =>this.links(value)} >
		<Item divider={true} label="Settings" value="Settings"/>
	        <Item label="Table Settings" icon="fa-search">
	            <Item label="SET TABLE" value="Table_Types" icon="fa-anchor"/>
	            <Item label="UPDATE TABLE" value="UPDATE TABLE" icon="fa-bar-chart"/> 
                <Item label="SET TABLE STATUS" value="SET TABLE STATUS" icon="fa-bar-chart"/>
	        </Item>
	        <Item label="Item 2" value="item2" icon="fa-automobile">
	          <Item label="Item 2.1" value="item2.1.1">
	            <Item label="Item 2.1.1" value="item2.1.1"/>
	            <Item label="Item 2.1.2" value="item2.1.2"/>
	          </Item>
	          <Item label="Item 2.2" value="item2.2"/>
	        </Item>
		<Item divider={true} label="Segment 2" value="segment2"/>
		<Item label="Item 3" value="item3" icon="fa-beer"/>
	      </SideMenu>
           
            
		</div>
			)
		}
}

export default Adminoptions
