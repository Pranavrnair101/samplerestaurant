import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel, Form } from 'react-bootstrap';
import {Table,Input,Button} from 'semantic-ui-react';
import IconButton from 'material-ui/IconButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Ionicon from 'react-ionicons';
import axios from 'axios';
import ReactCountdownClock from 'react-countdown-clock';
import './divstyles.css';
import "./main.css";
import  { Redirect } from 'react-router-dom';
import View_Order from './View_Order'
import Badge from 'material-ui/Badge';
import DeleteIcon from 'material-ui-icons/Delete';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {reducer_action_placeorder} from '../Actions/Reducer_action_placeorder';
import { Link } from 'react-router-dom';




class Make_Order extends Component{
    
    
    constructor(props){
        
        let tnum;
		super(props);
        
        this.handleChange=this.handleChange.bind(this);
		this.handleSubmit=this.handleSubmit.bind(this);
        this.onSort = this.onSort.bind(this);
        this.myorder=this.myorder.bind(this);
        this.placeorder= this.placeorder.bind(this);
        this. del_item_ordered=this.del_item_ordered.bind(this);
        this.quantityminus=this.quantityminus.bind(this);
        this.state = {
            totalrate:"",
            tab_number:props.table_number,
            rowdata:"",
            rowdata_o:[],
            selected_data_index:[],
            data:"",
          
            billnumber:"",
            quantity:[],
            status:[],
           
            
            //flag:0,
            //qtyupdindx:"",
            
            
			};
        
        
        }
   
    
     
    componentDidMount(){ 
 
        let _this=this
     
    axios.get('http://localhost:7777/api/itemsdata', { mode: 'no-cors'})
        .then( function(res){
        _this.setState({rowdata: res.data})
        console.log(_this.state.rowdata)   
            });
}
    
 
    handleSubmit(e){
		console.log('submited');
		e.preventDefault();   
	}
    
	handleChange(e){                               
        console.log('data changed');
        this.setState({
            [e.target.name]: e.target.value
            });
    }
    
    
    itemlist(e)
    {
        
        let _this=this
       
        
        console.log("loading the ordering function")
        
       
      e.preventDefault();
    axios.get('http://localhost:7777/api/itemsdata', { mode: 'no-cors'})
        .then( function(res){
       
        _this.setState({rowdata: res.data})
        console.log(_this.state.rowdata)
                
    
            });
  
        }
    
    onSort(event, sortKey){
    
    const rowdata = this.state.rowdata;
    rowdata.sort((a,b) => a[sortKey].localeCompare(b[sortKey]))
    this.setState({rowdata})
  }
    
    
    myorder(e)
    {
        
        e.preventDefault();
        let totalrate;
        let rowdatain;
        let _this=this;
        let qty=this.state.quantity
        
        let flag;
        let indx_sel_itm=e.currentTarget.getAttribute('data-key')
        let itm_selectd= this.state.rowdata[indx_sel_itm].item_name;
        let selected_data_indx=this.state.selected_data_index;
        
        let quantityupd= this.state.quantity;
       let newqty=1;
        let nqty=[];
      let st=this.state.status;
        let stt="preparing";
        console.log("loading the ordering function")
        if (this.state.selected_data_index.length>0)
            {
                for(var i=0; i< selected_data_indx.length; i++)
                        {    
                            if (indx_sel_itm===this.state.selected_data_index[i])
                                {                   
                                flag=1;
                                    //this.setState({flag:1,qtyupdindx:i})
                                    console.log("already selected item")
                                   /*let newqqtty=this.state.quantity
                                   let nq=Number(this.state.quantity[indx_sel_itm])+1
                                   newqqtty.splice(indx_sel_itm,1,nq)
                                    this.setState({quantity:newqqtty})*/
                                      for(var k=0; k<this.state.quantity.length; k++)
                                            {  console.log("entered in to for loop to update the qty array")
                                                let newqt=this.state.quantity[k]             
                                                if (i===k)
                                                    {
                                                        console.log("same item found need to increment qty")
                                                 newqt = Number(this.state.quantity[k])+1
                                                        console.log(newqt)
                                                    }
                                               nqty.push(newqt)
                                            }
                this.setState({quantity:nqty}) 
                                }
                        }
                if(flag!==1)
                               {
                                    console.log("new item")
                                    _this.setState({rowdata_o: _this.state.rowdata_o.concat(_this.state.rowdata[indx_sel_itm])})
                                    _this.setState({selected_data_index:selected_data_indx.concat(indx_sel_itm)})
                            _this.setState({quantity:qty.concat(newqty)})
                                   _this.setState({status:st.concat(stt)})
                                }
            }
        else{
                console.log("first item")
                let qt=1
                _this.setState({rowdata_o: _this.state.rowdata_o.concat(_this.state.rowdata[indx_sel_itm])})
                _this.setState({selected_data_index:selected_data_indx.concat(indx_sel_itm)})
            _this.setState({quantity:qty.concat(newqty)})
            _this.setState({status:st.concat(stt)})
            }
    console.log("the index is "+e.currentTarget.getAttribute('data-key'));
        console.log("the selected item is "+itm_selectd)
        console.log(_this.state.rowdata_o)
  }
    placeorder(e){
        e.preventDefault();
          let _this=this
         let ordered_items= this.state.rowdata_o.map((items)=>{
                                                 return items.item_name    
                                                     })  
         let preparation_time= this.state.rowdata_o.map((items)=>{
                                                 return items.preparation_time    
                                                     })  
        axios.get('http://localhost:7777/api/billnumber', { mode: 'no-cors'}) // to get the last bill number from db havw to work
            .then (function(res)
        {
            _this.setState({billnumber:res.data.billnumber});
            console.log("the bill number")
            console.log(res.data.billnumber)
        });
        
        
         /*for (var i=0; i< _this.state.rowdata_o.length; i++)
		{
        console.log("inside for loop to place order")
        axios.post('http://localhost:7777/api/postordereditemdata',  { 
       Itemordered: _this.state.rowdata_o[i].item_name,
            billnumber:"12344",
        })
        }*/
        
        console.log("place order click")
        axios.post('http://localhost:7777/api/postordereditemdata',  {
            table_num:"",
            billnumber:"12344",
            Itemordered:ordered_items,
            Preparation_time:preparation_time,
            Ordered_qty:this.state.quantity,
            status:this.state.status,
            
        })
        let data=[{table_num:"",
            billnumber:"12344",
            Itemordered:ordered_items,
            Preparation_time:preparation_time,
            Ordered_qty:this.state.quantity,
            status:this.state.status}]
        
       this.props.dispatch(reducer_action_placeorder(data))
 this.props.history.push('/View_Order')
        return(
        <div>
             <View_Order/>
            </div>
        )
       
       
    }
    
    del_item_ordered(e){
        e.preventDefault();
        let i= e.currentTarget.getAttribute('data-key')
        console.log(this.state.rowdata_o)
        console.log(this.state.selected_data_index)
        console.log(this.state.quantity)
        let itemarray= this.state.rowdata_o
        let qtyaray= this.state.quantity
        let selected_indexes=this.state.selected_data_index
        let st=this.state.status;
        itemarray.splice(i,1)
        qtyaray.splice(i,1)
        selected_indexes.splice(i,1)
        st.splice(i,1)
        this.setState({rowdata_o:itemarray})
        this.setState({quantity:qtyaray})
        this.setState({selected_data_index:selected_indexes})
        this.setState({state:st})
    }

    quantityminus(e){
        e.preventDefault();
        let i= e.currentTarget.getAttribute('data-key')
         let qtyaray= this.state.quantity
         let nq= qtyaray[i]
         if (nq===1){
             alert("you may delete the item as the qunatity cannot be set to 0")
         }
        else{ qtyaray.splice(i,1,Number(nq)-1)
        this.setState({quantity:qtyaray})
            }
        
    }
    
    render () {
         let datas=this.state.rowdata
         let data=[]
         let datas_o=this.state.rowdata_o
         let data_o=[]
         let selected_data=[]
        let total_rate=0;
        let nqty=[];
        console.log("the table name in renderin makeoreder is ")
        console.log(this.props.table_number)
        /*if( this.state.flag===1)
            {
                for(var k=0; k<this.state.quantity.length; k++)
                                            {  console.log("entered in to for loop to update the qty array")
                                                let newqt=this.state.quantity[k]             
                                                if (this.state.qtyupdindx===k)
                                                    {
                                                        console.log("same item found need to increment qty")
                                                 newqt = Number(this.state.quantity[k])+1
                                                        console.log(newqt)
                                                      
                                                    }
                                               nqty.push(newqt)
                                            //this.setState({quantityupd:this.state.quantityupd.concat(newqt)})  
                                            }
                console.log("the new array is ")    
                console.log(nqty)    
                this.setState({flag:0})
                this.setState({quantity:nqty})
               console.log(this.state.quantity)
            }*/
       
        for (var i=0; i< datas.length; i++)
		{  
      data.push(
                <tr  key={i} data-key={i} onClick={this.myorder} >
                <td>  {datas[i].item_name}</td>                
                <td>  {datas[i].rate}</td>                
                <td>  {datas[i].preparation_time} Minutes</td>
                </tr> 
                );
        }
         for (var i=0; i< datas_o.length; i++)
        {
            data_o.push(
                <tr > 
                <Badge color="primary" badgeContent={this.state.quantity[i]}><td>  {datas_o[i].item_name}</td></Badge>                
                <td>  {datas_o[i].rate}</td>                
                <td>  {datas_o[i].preparation_time} Minutes</td>
                <td>  {this.state.quantity[i]}</td>
                <td>  <Button className="btn-sm btn-danger" data-key={i}onClick={this.quantityminus}>-</Button> </td>
                <td > <DeleteIcon data-key={i} onClick={this.del_item_ordered}></DeleteIcon> </td>
                </tr> 
            );
        }
         for (var i=0; i< datas_o.length; i++)
        {
            total_rate= (Number(datas_o[i].rate)*this.state.quantity[i]) + Number(total_rate)
        }
      
        return(
            <div>
            <div>
          
            <div>
            
        <Button color='dark blue'> SELECT DISH</Button>
            </div>
            <Table inverted>
            <Table.Header>
								< Table.Row > 
                                    <Table.HeaderCell>Item Name </Table.HeaderCell>
									<Table.HeaderCell > Rate </Table.HeaderCell>
									<Table.HeaderCell>  Time to Prepare </Table.HeaderCell>
									
								</Table.Row>
            </Table.Header>
							<Table.Body >{data}</Table.Body>
    
						
             </Table>
            
  </div>
          <div>
      
            <Button color='dark blue' > SELECTED DISH</Button>
                </div>
                <Table inverted>
                <Table.Header>
                                    <Table.Row > 
                                        <Table.HeaderCell>ItemName </Table.HeaderCell>
                                        <Table.HeaderCell> Rate </Table.HeaderCell>
                                        <Table.HeaderCell>  Time to Prepare </Table.HeaderCell>
                                        <Table.HeaderCell> qty </Table.HeaderCell>
                                
                                    </Table.Row>
                </Table.Header>
                                <Table.Body > {data_o} </Table.Body>
        
                    
                </Table>
              
             
               <Input disabled> RATE : {total_rate} </Input>
                <div>
              < Button color='black' onClick={this.placeorder}> Place Order </Button> 
                </div>
                
             
          </div>


            
          
        
        );
    }
}

// Anything returned from this function will end up as props
// on the BookList container
function mapDispatchToProps(dispatch) {
  // Whenever selectBook is called, the result shoudl be passed
  // to all of our reducers
  return bindActionCreators({ reducer_action_placeorder: reducer_action_placeorder }, dispatch);
}

// Promote BookList from a component to a container - it needs to know
// about this new dispatch method, selectBook. Make it available
// as a prop.
export default connect( mapDispatchToProps)(Make_Order);



   