import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel, Form ,Table} from 'react-bootstrap';
import IconButton from 'material-ui/IconButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Ionicon from 'react-ionicons';
import axios from 'axios';
import "./main.css";
import ReactCountdownClock from 'react-countdown-clock'; 

import { connect } from "react-redux";



class View_Order extends Component{
    constructor(props){
		super(props);
        this.handleChange=this.handleChange.bind(this);
		this.handleSubmit=this.handleSubmit.bind(this);
		//this.setstatus=this.setstatus.bind(this);
     
        this.state = {
            status: [],
            data:[],
            rowdata:[],
            preparationtime:[],
            items:[]
			};
        }
    

    
    componentDidMount(){ 
        //all the data here need to passed using redux to avoid fetching data from database
        let _this=this;
        this.setState({status:"preparing"})
     axios.post('http://localhost:7777/api/postordereditemdata_view',{billnumber:this.props.billnumber}).then(function(res) {  
         let list=res.data[0]
         _this.setState({items:Object(list.Itemordered)});
         _this.setState({preparationtime:Object(list.Preparation_time)});
         _this.setState({status:Object(list.status)});
         _this.setState({rowdata:Object(list)});
       
})
			.catch(function(error) {
				console.log(error);
			}); 
    }
    
    
    handleSubmit(e){
		console.log('submited');
		e.preventDefault();   
	}
    
    setstatus(i){
     let newarray= this.state.status
     console.log(newarray)
    newarray[i]="send to table"
    this.setState({status:newarray})   
    }
    
    
    
    
    
	handleChange(e){
        console.log('data changed');
        this.setState({
            [e.target.name]: e.target.value
            });
    }
   
    

    

	render () {
         let data=[];
         
         let rowdatas=[];
        let _this=this;
        let rowdata=this.state.rowdata;
        let items=this.state.items;
        let preparationtime=this.state.preparationtime;
        let status=this.state.status;
        console.log("the date from make oredr is ")
        console.log(this.props.data)
        console.log(rowdata)
        console.log(Object(rowdata.Itemordered).length)
        console.log(this.state.status)
         for (var i=0; i< Object(rowdata.Itemordered).length; i++)
		{
         rowdatas.push(
                <tr>
                <td>  {items[i]}</td>                
                <td> {preparationtime[i]} Minutes</td>  
             <td> <ReactCountdownClock 
                    seconds={preparationtime[i]}
                     color="blue"
                     alpha={1}
                     size={100}
                    onComplete={this.setstatus.bind(this,i)}
                  /></td>
                  <td> <Button bsStyle="danger" bsSize="xsmall"  >  
            <Ionicon icon="ios-add-circle" fontSize="30px" color="red" rotate={true}/>
          {this.state.status[i]}
            </Button> </td>             
                </tr> 
            );
        }
        
        
        return(
            <div class="vieworder">
                  <div className="container">
            		<Table className="table table-striped table-dark">
							<thead>
								< tr > 
                                    <th>Items Ordered </th>
									<th> Time to prepare </th>
									<th> Time pending </th>
									<th> Current State </th>
								</tr>
							</thead>
							<tbody >{rowdatas}</tbody>
    
						</Table>
                  </div>
            </div>
            );
    
    
    }
}

function mapStateToProps(state) {
  return {
    data: state.data
  };
}

export default connect(mapStateToProps)(View_Order);