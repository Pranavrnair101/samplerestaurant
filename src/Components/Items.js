import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel, Form } from 'react-bootstrap';
import { Divider, Responsive, Grid, Segment, Button, Input, Table, Icon } from 'semantic-ui-react';

import axios from 'axios';
import "./main.css";

class Items extends Component {
    constructor(props) {

        super(props);
        this.addnewitem = this.addnewitem.bind(this)
        this.removeitem = this.removeitem.bind(this);
        this.submititems = this.submititems.bind(this);
        this.addsingleitem = this.addsingleitem.bind(this);
        this.submitsigleitem = this.submitsigleitem.bind(this);

        this.state = {
            new_items: [],
            new_item: "",
        };
    }



    componentDidMount() {
    }


    handleSubmit(e) {
    }


    handleChange(e) {
        console.log('data changed');
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    addnewitem() {
        let _this = this;
        let new_itemarry;
        /* let item= document.getElementById('itemname').value;
         let rte=  document.getElementById('rate').value;   
         let pretime=  document.getElementById('p_time').value;
                 new_item.push(item)
                 new_item.push(rte)
                 new_item.push(pretime)
              new_itemarry= Array(new_item)*/

        new_itemarry = [{ "item_name": document.getElementById('itemname').value, "rate": document.getElementById('rate').value, "preparation_time": document.getElementById('p_time').value }]
        _this.setState({ new_items: _this.state.new_items.concat(new_itemarry) })
        var testElements = document.getElementsByClassName("item");
        var testDivs = Array.prototype.filter.call(testElements, function (testElement) {
            testElement.value = "";
        });
    }
    removeitem(e) {
        e.preventDefault();
        let _this = this;
        let i = e.currentTarget.getAttribute('data-key');
        let itms = this.state.new_items
        itms.splice(i, 1)
        this.setState({ new_items: itms })


    }
    submititems() {
        axios.post('http://localhost:7777/api/postnewitems', {
            items: this.state.new_items
        })
    }
    addsingleitem() {
        console.log("mouseenter")
        let _this = this;
        let new_itemarry;
        new_itemarry = [{ "item_name": document.getElementById('itemname').value, "rate": document.getElementById('rate').value, "preparation_time": document.getElementById('p_time').value }]
        _this.setState({ new_item: new_itemarry })

    }
    submitsigleitem() {
        console.log("mouseclicked")
        axios.post('http://localhost:7777/api/postnewitems', {
            items: this.state.new_item
        })
        var testElements = document.getElementsByClassName("item");
        var testDivs = Array.prototype.filter.call(testElements, function (testElement) {
            testElement.value = "";
        });
    }



    render() {
        console.log("new item")
        console.log(this.state.new_item)
        console.log("new items")
        console.log(this.state.new_items)
        let data = [];
        let tabview;
        let addbutton
        for (var i = 0; i < this.state.new_items.length; i++) {
            data.push(
                <tr key={i}  >
                    <td>  {this.state.new_items[i].item_name}</td>
                    <td>  {this.state.new_items[i].rate}</td>
                    <td>  {this.state.new_items[i].preparation_time} Minutes</td>
                    <td > <Button data-key={i} color='black' onClick={this.removeitem}>Del</Button></td>
                </tr>
            );
        }
        if (this.state.new_items.length === 0) {
            tabview = ""

            addbutton = <div><Button color='black' onMouseEnter={this.addsingleitem} onClick={this.submitsigleitem}>ADD ITEM </Button></div>
        }
        else {
            <div className="padding">

                <div id="table">
                    <Table inverted>
                        <Table.Header>

                            < Table.Row>
                                <Table.HeaderCell>Item Name </Table.HeaderCell>
                                <Table.HeaderCell > Rate </Table.HeaderCell>
                                <Table.HeaderCell>  Time to Prepare </Table.HeaderCell>

                            </Table.Row>
                        </Table.Header>
                        <Table.Body >{data}</Table.Body>


                    </Table>
                    <Button color='black' onClick={this.submititems}>ADD ITEMS </Button>
                </div>
            </div>

            addbutton = "";
        }
        return (


            <div className="overall font padding background fontcolor">
                <Segment.Group>
                    <Responsive as={Segment} minWidth={880}>


                        <div>
                            <Grid columns='equal' relaxed>
                                <Grid.Column>
                                    <Segment basic className='paditems'>
                                        Item Name<Input focus size='mini' placeholder='Item Name' type="text" name="item" className="item" id="itemname" /> </Segment>
                                </Grid.Column>
                                <Grid.Column>
                                    <Segment basic className='paditems'>
                                        Rate<Input focus size='mini' placeholder='Rate' type="text" name="item" className="item" id="rate" /></Segment>
                                </Grid.Column>
                                <Grid.Column>
                                    <Segment basic className='paditems'>
                                        Preparation Time<Input focus size='mini' placeholder='Preparation Time' type="text" className="item" name="item" id="p_time" /></Segment>
                                </Grid.Column>
                                <Grid.Column>
                                    <Segment basic className='paditems'>
                                        <Icon className='padd' size='big' name='plus circle' color="black" data-key={i} onClick={this.addnewitem}> </Icon> </Segment>
                                </Grid.Column>
                            </Grid>

                        </div>
                    </Responsive>
                </Segment.Group>

                <Segment.Group>
                    <Responsive as={Segment} maxWidth={880}>
                        <Grid divided='vertically'>
                            <Grid.Row columns={1}>
                                <Grid.Column>
                                    <Segment basic className='paditems'>
                                        Item Name<Input focus size='mini' placeholder='Item Name' type="text" name="item" className="item" id="itemname" /> </Segment>
                                </Grid.Column>

                            </Grid.Row>

                            <Grid.Row columns={1}>
                                <Grid.Column>
                                    <Segment basic className='paditems'>
                                        Rate<Input focus size='mini' placeholder='Rate' type="text" name="item" className="item" id="rate" /></Segment>
                                </Grid.Column>

                            </Grid.Row>
                            <Grid.Row columns={1}>
                                <Grid.Column>
                                    <Segment basic className='paditems'>
                                        PreparationTime<Input focus size='mini' placeholder='Preparation Time' type="text" name="item" className="item" id="p_time" /></Segment>
                                </Grid.Column>

                            </Grid.Row>
                        </Grid>
                    </Responsive>
                </Segment.Group>

                <Divider horizontal>
                    <div className='addbutton'>
                        {addbutton} </div>
                </Divider>

            </div>



        );
    }
}

export default Items
