import React, { Component } from 'react';
import {Grid,Responsive,Segment,Divider,Table,Button,Checkbox,Input} from 'semantic-ui-react';

import axios from 'axios';
import { Link } from 'react-router-dom';
import  { Redirect } from 'react-router-dom';

import { withStyles } from 'material-ui/styles';
import './main.css';
import Delete from 'material-ui-icons/Delete';
import DeleteIcon from 'material-ui-icons/Delete';
import Icon from 'material-ui/Icon';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';

let tabs=[]
class Table_Types extends Component{
    
	constructor(props){
		super(props);
        this.toggle=this.toggle.bind(this)
        this.add_table=this.add_table.bind(this)
        this.deletetabletype=this.deletetabletype.bind(this);
          this.edittabletype=this.edittabletype.bind(this)
          this.handleClose=this.handleClose.bind(this)
          this.handleChange=this.handleChange.bind(this)
this.state = {
		checkedtypes:[],
        num_tables:[],
        rowdata:"",
        updrowdata:"",
    checkboxState:true,
    edittable:false,
    open: false,
    toupdate:"",
    toupdateval:""
		};
}
    
    
componentDidMount(){
    let _this=this
    axios.get('http://localhost:7777/api/tabletypes', { mode: 'no-cors'})
        .then( function(res){
        _this.setState({rowdata: res.data})
        
    })
}

componentDidUpdate(nextState){
        return this.state.rowdata
    }


handleSubmit(e){
		console.log('submited');
		e.preventDefault();	
	}
    
    
toggle(e){
        this.setState({
      checkboxState: !this.state.checkboxState
        })
        let selcedchecks=[]
        let testElements = document.getElementsByClassName(e.target.name);
         let tab_typ=testElements[0].name
          if(e.target.checked===true)
              {   
                        selcedchecks.push(tab_typ)
                         this.setState({checkedtypes:this.state.checkedtypes.concat(selcedchecks)})       
              }
        else {
            selcedchecks = this.state.checkedtypes.filter(function(e) { return e !== tab_typ })
                                 this.setState({checkedtypes:selcedchecks})
        }
    }
    
	handleChange(e){
        console.log(e.target.value)
        this.setState({
          toupdateval: e.target.value
            });
       
		
	}
    
    
    add_table(){
       let _this=this
        let num_tab_array=[]
         let new_checked_array=[]
        for (let i=0; i<this.state.checkedtypes.length; i++)
		{  
            console.log("the lenght of checked arry is ")
            console.log(this.state.checkedtypes.length)
            console.log(this.state.checkedtypes[i])
           let num_tabs= document.getElementById(this.state.checkedtypes[i]+"tb").value
         
            if (num_tabs>0){
               num_tab_array.push(num_tabs) 
               new_checked_array.push(this.state.checkedtypes[i])
            }
        
                     document.getElementById(this.state.checkedtypes[i]+"tb").value=""
                     document.getElementById(this.state.checkedtypes[i]).checked=false
            
       
        }
        this.setState({checkedtypes:new_checked_array})  
          this.setState({num_tables:num_tab_array}) 
                   console.log(this.state.num_tables  )
                axios.post('http://localhost:7777/api/addtabletype',  { 
                    tab_type:new_checked_array,
                    num:num_tab_array 
                    }).then (function(res)
                        {
                            axios.get('http://localhost:7777/api/tabletypes', { mode: 'no-cors'})
                                .then( function(res){
                                _this.setState({rowdata: res.data})
                              console.log(res.data  )
                            })
                        });
     axios.post('http://localhost:7777/api/upd_tab_status', { 
                            table_types:new_checked_array, no_tabs: num_tab_array
                            })
    }
    
    edittabletype(e){
            e.preventDefault();
        let i= e.currentTarget.getAttribute('data-key')
        let val=this.state.rowdata[i].no_table
        console.log(i);
        this.setState({open:true,toupdate:this.state.rowdata[i].table_type,toupdateval:val})
       
         
    }
    handleClose() {
        let _this=this
        console.log("inside handleclose")
        console.log(this.state.toupdate)
    this.setState({open:false});
       let no_tab= document.getElementById("no_tables").value
       if (no_tab==="0"||no_tab===null){
                  alert("cannot be set ")
                    }
        else{
            axios.post('http://localhost:7777/api/updatetabletype', { 
                            table_type:this.state.toupdate,no_table:no_tab
                            }).then(function(res){
                                        axios.get('http://localhost:7777/api/tabletypes', { mode: 'no-cors'})
                                .then( function(res){
                                _this.setState({rowdata: res.data})
                                        })
                                });
                }
  }
    deletetabletype(e){
        let _this=this
        e.preventDefault();
        let val= e.currentTarget.name
       axios.post('http://localhost:7777/api/deletetabletype', { 
                            table_type:val
                            }).then (function(res)
                        {
                            axios.get('http://localhost:7777/api/tabletypes', { mode: 'no-cors'})
                                .then( function(res){
                                _this.setState({rowdata: res.data})
                              console.log(res.data  )
                            })
                        })
        axios.post('http://localhost:7777/api/deletetablestatus', { 
                            table_type:val
                            })
                                let testElements = document.getElementsByClassName(val);
                                let testDivs = Array.prototype.filter.call(testElements, function(testElement){
                                testElement.disabled = false;
                                testElement.checked = false;
                                        });
    }
    
render () {
let rowdatas=[]; 
let rowdata=this.state.rowdata;
let tabtypes=["2 seater","4 seater","6 seater","8 seater"]
let lasttype=[];
  for (let i=0; i< rowdata.length; i++)
		{  
            rowdatas.push( 
                            <tr>  
                            <td>  {rowdata[i].table_type}</td>                
                            <td>  {rowdata[i].no_table}</td>  
                            <td> <Button data-key={i} onClick={this.edittabletype} variant="fab" mini color="primary" >
                            <Icon>edit</Icon> </Button>  </td>
                                                        
                              
                            <td> <Button variant="fab" mini color="secondary" data-key={i} onClick={this.deletetabletype} name={rowdata[i].table_type}>
                                <DeleteIcon /> 
                                </Button> </td>  
                                </tr>
                        );  
            if (rowdata[i].table_type==="2 seater"||rowdata[i].table_type==="4 seater"||rowdata[i].table_type=="6 seater"||rowdata[i].table_type=="8 seater")
                {
           
                 let testElements = document.getElementsByClassName(rowdata[i].table_type);
                 let testDivs = Array.prototype.filter.call(testElements, function(testElement){
                                                             testElement.disabled = true;
                                                            testElement.checked = true;
                     
                                                            });
                }
        }// for end
        
    
   


return(
    <div>
    <Segment.Group>
    <Responsive as={Segment} minWidth={881}>
 
  
    <Grid columns={4} relaxed>
    <Grid.Column>
    <Segment basic>
    <Checkbox  name="2 seater" class="2 seater" id="2 seater" value="2 seater" onChange={this.toggle}/>2 SEATER
   
    <div ClassName="col"><Input size='mini' focus placeholder='numbers' type="text"  key="1" class="2 seater" id="2 seatertb"  /> </div>
    </Segment>
    </Grid.Column>
    
    <Grid.Column>
    <Segment basic>  
<Checkbox key="2" name="4 seater" id="4 seater" class="4 seater"  value="4 seater" onChange={this.toggle} />4 SEATER 

    <div ClassName="col"><Input size='mini' focus placeholder='numbers' type="text"  key="2" class="4 seater" id="4 seatertb" /> </div>
    </Segment>
    </Grid.Column>
    <Grid.Column>
    <Segment basic>   
    <Checkbox  name="6 seater" class="6 seater" id="6 seater" value="6 seater" onChange={this.toggle}/>6 SEATER 
  
     <div ClassName="col"><Input size='mini' focus placeholder='numbers' type="text" key="3" class="6 seater" id="6 seatertb"  /> </div>
     </Segment>
    </Grid.Column>
<Grid.Column>
    <Segment basic>   
    <Checkbox  name="8 seater" class="8 seater" id="8 seater" value="8 seater" onChange={this.toggle}/>8 SEATER 
     <div ClassName="col"><Input size='mini' focus placeholder='numbers' type="text"  key="4" class="8 seater" id="8 seatertb" /> </div>
     </Segment>
    </Grid.Column>
    </Grid>
    </Responsive>
  </Segment.Group>  
  <Segment.Group>
    <Responsive as={Segment} maxWidth={880}>

  <Grid divided='vertically'>
    <Grid.Row columns={1}>
      <Grid.Column>
      <Segment basic>
    <Checkbox name="2 seater" class="2 seater" id="2 seater" value="2 seater" onChange={this.toggle}/>2 SEATER
   
    <div ClassName="col"><Input size='mini' focus placeholder='numbers' type="text"  key="1" class="2 seater" id="2 seatertb"  /> </div>
    </Segment>
      </Grid.Column>
    
    </Grid.Row>

    <Grid.Row columns={1}>
      <Grid.Column>
      <Segment basic>  
<Checkbox key="2" name="4 seater" id="4 seater" class="4 seater"  value="4 seater" onChange={this.toggle} />4 SEATER 

    <div ClassName="col"><Input size='mini' focus placeholder='numbers' type="text"  key="2" class="4 seater" id="4 seatertb" /> </div>
    </Segment>
      </Grid.Column>
     
    </Grid.Row>

    <Grid.Row columns={1}>
      <Grid.Column>
      <Segment basic>   
    <Checkbox  name="6 seater" class="6 seater" id="6 seater" value="6 seater" onChange={this.toggle}/>6 SEATER 
  
     <div ClassName="col"><Input size='mini' focus placeholder='numbers' type="text" key="3" class="6 seater" id="6 seatertb"  /> </div>
     </Segment>
      </Grid.Column>
     
    </Grid.Row>
  </Grid> 
  </Responsive>
  </Segment.Group> 
    <Divider horizontal>
    <Button color='black' onClick={this.add_table}>Add Table </Button>
    </Divider>
 

    
    <div className="row">
    <Table inverted>
							<Table.Header>
								< Table.Row > 
                                    <Table.HeaderCell>Table Types</Table.HeaderCell>
									<Table.HeaderCell> No: of Tables </Table.HeaderCell>
                                    <Table.HeaderCell></Table.HeaderCell>
                                    <Table.HeaderCell></Table.HeaderCell>
									
								</Table.Row>
							</Table.Header>
							<Table.Body >{rowdatas}</Table.Body>
    
						</Table>
    </div>
    <div>
                                                <Dialog
                                                  open={this.state.open}
                                                  onClose={this.handleClose}
                                                  aria-labelledby="form-dialog-title">
                                                  <DialogTitle id="form-dialog-title">Edit number of tables</DialogTitle>
                                                  <DialogContent>
                                                    <DialogContentText>
                                                      
                                                    </DialogContentText>
                                                    <TextField
                                                      autoFocus
                                                      margin="dense"
                                                      id="no_tables"
                                                      name="no_tables"
                                                      label="Number of tables"
                                                      type="text"
                                                    value={this.state.toupdateval}
                                                    onChange={this.handleChange}
                                                      fullWidth
                                                    />
                                                  </DialogContent>
                                                  <DialogActions>
                                                    <Button onClick={this.handleClose} color="primary">
                                                     SET
                                                    </Button>
                                                  </DialogActions>
                                                </Dialog>
    </div>
    </div>

    );
}
    }
export default Table_Types;