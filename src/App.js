import React, { Component } from 'react';
import PropTypes from 'prop-types';
import logo from './logo.svg';
import { Button, Tab, Header, Icon, Image, Menu, Segment, Sidebar } from 'semantic-ui-react'
import './App.css';
import "./Components/main.css";

import View_Order from './Components/View_Order';
import Make_Order from './Components/Make_Order';
import Select_Table from './Components/Select_Table';
import Table_Allocation from './Components/Table_Allocation';
import Main from './Main';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Adminoptions from './Components/Adminoptions';
import Divtest from './divtest';
import Table_Types from './Components/Table_Types';
import Items from './Components/Items';
import Sidemenu from './Components/Sidemenu';



const VerticalSidebar = ({ animation, direction, visible }) => (
  <Sidebar class='background'
    as={Menu}
    animation={animation}
    direction={direction}
    icon='labeled'
    inverted
    vertical
    visible={visible}
    width='thin'
  >
    <Menu.Item as='a'>
      <Icon name='home' />
      Home
    </Menu.Item>
    <Menu.Item as='a'>
      <Icon name='gamepad' />
      Games
    </Menu.Item>
    <Menu.Item as='a'>
      <Icon name='camera' />
      Channels
    </Menu.Item>
  </Sidebar>
)

VerticalSidebar.propTypes = {
  animation: PropTypes.string,
  direction: PropTypes.string,
  visible: PropTypes.bool,
}





class App extends Component {
  state = {
    animation: 'overlay',
    direction: 'left',
    dimmed: false,
    visible: false,
  }
  handleAnimationChange = animation => () =>
    this.setState({ animation, visible: !this.state.visible })

  handleDimmedChange = (e, { checked }) => this.setState({ dimmed: checked })

  handleDirectionChange = direction => () => this.setState({ direction, visible: false })

  render() {
    const { animation, dimmed, direction, visible } = this.state
    const vertical = direction === 'bottom' || direction === 'top'

    const panes = [
      { menuItem: 'Table types', render: () => <Tab.Pane attatched={false}> <Table_Types /></Tab.Pane> },
      { menuItem: 'Select table', render: () => <Tab.Pane attatched={false}> <Select_Table /></Tab.Pane> },
      { menuItem: 'Items', render: () => <Tab.Pane attatched={false}>  <Items /></Tab.Pane> },
      { menuItem: 'Table_Allocation', render: () => <Tab.Pane attatched={false}>  <Table_Allocation /></Tab.Pane> },
      { menuItem: 'View Order', render: () => <Tab.Pane attatched={false}>  <View_Order /></Tab.Pane> },

    ]
    return (
      <div className='background'>
        <Icon size='big' name="sidebar" onClick={this.handleAnimationChange('scale down')}></Icon>

        <Sidebar.Pushable as={Segment} class='background'>
          {vertical ? null : (
            <VerticalSidebar animation={animation} direction={direction} visible={visible} />
          )}
          <Sidebar.Pusher dimmed={dimmed && visible}>
            <Segment basic className='background'>

              <div>
                <Tab className='background' menu={{ secondary: true }} panes={panes} />
              </div>
            </Segment>
          </Sidebar.Pusher>
        </Sidebar.Pushable>


      </div>
    );
  }
}

export default App;
