import React from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import View_Order from './Components/View_Order';
import Make_Order from './Components/Make_Order';
import Select_Table from './Components/Select_Table';
import Table_Types from './Components/Table_Types';
import "./Components/main.css";
import {Button,Icon} from "semantic-ui-react";  
// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /roster
// and /schedule routes will match any pathname that starts
// with /roster or /schedule. The / route will only match
// when the pathname is exactly the string "/"
const Main = () => (
  <main>
    <div>
      <Button color='facebook'>
        <Icon name='facebook' /> Facebook
    </Button>

      <Switch>
        <div className="container overall">
          <Route path='/View_Order' component={View_Order} />
          <Route path='/Make_Order' component={Make_Order} />
          <Route path='/Table_Types' component={Table_Types} />
        </div>
      </Switch>
    </div>
  </main>
)
export default Main
